package main

import (
	"testing"
)

func TestMakeDefStructName(t *testing.T) {
	res := makeDefStructName("DaTa")
	if res != "DefImplDaTa" {
		t.Errorf("expected %s but got %s", "DefImplDaTa", res)
	}
}

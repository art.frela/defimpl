# defimpl



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/art.frela/defimpl.git
git branch -M main
git push -uf origin main
```

Go utility for generating default implementation of interfaces

## usage

```bash
go install gitlab.com/art.frela/defimpl@latest

defimpl [loglevel: debug|info|warn|error, default=info] ./path/to/fileWithInterface.go
# в результате должен быть создан файл ./path/to/fileWithInterface_defimpl.go содержащий дефолтные реализации интерфейсов
```

Допустим в файле с именем `handler.go` есть интерфейс

```go
type Handler interface {
    Do() error
    Get() (interface{}, error)
}
```

тогда при выполнении команды `defimpl handler.go` создавался бы файл в этой же папке с именем `handler_defimpl.go` у которого внутри было бы имя того же пакета, необходимые импорты и структура с методами:  

```go
type DefImplHandler struct{}

func (DefImplHandler) Do() error {
    panic("method Do doesn't implemented")
}

func (DefImplHandler) Get() (interface{}, error) {
    panic("method Get doesn't implemented")
}
```

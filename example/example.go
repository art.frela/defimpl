package example

import (
	"encoding/json"
	"encoding/xml"
	"image"
	"time"

	"gitlab.com/art.frela/defimpl/pkg/bar"
	dom "gitlab.com/art.frela/defimpl/pkg/domain"
)

//go:generate defimpl example.go

type Data interface {
	// Write is example write method
	Write([]byte, json.Decoder) (int, time.Time, error)
	// Read is example read method
	Read(...int) ([]byte, error)
	// Read is example read method
	ReadPtr(...*int) ([]byte, error)
	//
	ReadFuncs(...*xml.Decoder) ([]byte, error)
	//
	GoRoutine(in <-chan string, cancel <-chan struct{})
	//
	StructArg(d ...Text)
	// FindText is example findtext method
	FindText(<-chan string) (Text, error)
	// SendText is example send ptr to text to chan
	SendText(msg Text, out chan<- *Text)
	// Foo is example foo
	Foo(uint64, bool, *string) (*Text, *string, []byte)
	// Bar is example bar
	Bar(map[string]*Text) map[chan string]bool
	// GetMany is example many args and results
	GetMany(one, two, three int, txt, note Text) ([]Text, []Text, int64, error)
	//
	GetImages(lens ...string) ([]image.Image, error)
}

type Text struct {
	// Title is example title description
	Title string
	// Body is example body description
	Body string
	Bar  bar.Bar
}

type Handler interface {
	Do(in <-chan int, out chan<- *int) error
	Get(in chan int) (interface{}, error)
	See() chan Text
	Try() *dom.Zero
}
